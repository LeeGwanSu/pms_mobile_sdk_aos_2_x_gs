# Document Information
--
* PMS InBox Guide (Android)
* Creat Date : 2014.11.10 (Last Update)
* Drafter : Yang Dong Min

## Change History
* 2014.11.10 Yang Dong Min
	- Initially

# Table of Contents
--
1. Database (SQLite) related method
  - public Cursor selectMsgGrpList()
  - public MsgGrp selectMsGrp(String msgCode)
  - public int selectNewMsgCnt()
  - public Cursor selectMsgList(int page, int row)
  - public Cursor selectMsgList(String msgCode)
  - public Msg selectMsgWhereMsgId (String msgId)
  - public Msg selectMsgWhereUserMsgId (String userMsgID)
  - public long updateMsgGrp(String msgCode, ContentValues values)
  - public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId)
  - public long updateReadMsgWhereUserMsgId(String userMsgId)
  - public long updateReadMsgWhereMsgId(String msgId)
  - public long deleteUserMsgId(String userMsgId)
  - public long deleteMsgId(String MsgId)
  - public long deleteMsgGrp(String msgCode)
  - public long deleteExpireMsg()
  - public void deleteEmptyMsgGrp()
  - public void deleteAll()
2. Msg Class Architecture
3. Sample Code


## 1. Database (SQLite) related method
**This section describes the methods to access the SQLite to be used in the SDK.**


--
### - public Cursor selectMsgGrpList()
--
**Gets the message group list.**

* return
	- Cursor


--
### - public MsgGrp selectMsGrp (String msgCode)
--
**Gets the message group for that message group code.**

* parameter
	- String msgCode

* return
	- MsgGrp


--
### - public int selectNewMsgCnt()
--
**Gets the count of new messages.**

* return
	- int


--
### - public Cursor selectMsgList (int page, int row)
--
**Gets the message list.**

* parameter
   - int page
   - int row

* return
   - Cursor


--
### - public Cursor selectMsgList (String msgCode)
--
**Gets a list of messages for the appropriate message group code.**

* parameter
	- String msgCode

* return
	- Cursor


--
### - public Msg selectMsgWhereMsgId (String msgId)
--
**Gets the message value for that message ID.**

* parameter
	- String msgId

* return
	- Msg


--
### - public Msg selectMsgWhereUserMsgId (String userMsgId)
--
**Gets the message value for the appropriate user message ID.**

* parameter
	- String userMsgId

* return
	- Msg


--
### - public long updateMsgGrp (String msgCode, ContentValues values)
--
**Update the message group.**

* parameter
	- String msgCode
	- ContentValues values

* return
	- long


--
### - public long public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId)
--
**Updates to read the message.**

* parameter
	- String msgGrpCd
	- String firstUserMsgId
	- String lastUserMsgId

* return
	- long


--
### - public long updateReadMsgWhereUserMsgId (String userMsgId)
--
**Upgrade to read a message corresponding to the user message ID.**

* parameter
	- String userMsgId

* return
	- long


--
### - public long updateReadMsgWhereMsgId (String msgId)
--
**Upgrade to read a message corresponding to the message ID.**

* parameter
	- String msgId

* return
	- long


--
### - public long deleteUserMsgId (String userMsgId)
--
**Delete the message corresponding to the user message id.**

* parameter
	- String userMsgId

* return
	- long

	
--
### - public long deleteMsgId (String MsgId)
--
**Delete the message corresponding to the message id.**

* parameter
	- String msgId

* return
	- long


--
### - public long deleteMsgGrp (String msgCode)
--
**Delete the message group.**

* parameter
	- String msgCode

* return
	- long


--
### - public long deleteExpireMsg()
--
**The validity delete the last message.**


--
### - public void deleteEmptyMsgGrp()
--
**Delete the message group is empty.**


--
### - public void deleteAll()
--
**Removes all the data.**


## 2. Msg Class Architecture
**All push messages are stored in the Message DB (SQLite).**

**Msg class of the Message DB is the data is stored in the form bean.**

**Msg class can be used as follows**
~~~java
Cusror c = PMS.getInstance(context).selectMsgList(1, 1000);
c.moveToPosition(10);
Msg msg = new Msg(c);
System.out.println(msg.id);
~~~

**Msg class of variables as follows**

<table>
<tr><td>Variables</td><td>Explanation</td></tr>
<tr><td>id</td><td>DB's row ID</td></tr>
<tr><td>userMsgId</td><td>(Of the server) unique ID of the message</td></tr>
<tr><td>msgGrpName</td><td>Group name in the message</td></tr>
<tr><td>appLink</td><td>AppLink (additional data)</td></tr>
<tr><td>iconName</td><td>IconName (additional data)</td></tr>
<tr><td>msgId</td><td>Message ID</td></tr>
<tr><td>pushTitle</td><td>Push title</td></tr>
<tr><td>pushMsg</td><td>Push message</td></tr>
<tr><td>msgText</td><td>Message text</td></tr>
<tr><td>map1</td><td>Add the mapping information 1</td></tr>
<tr><td>map2</td><td>Add the mapping information 2</td></tr>
<tr><td>map3</td><td>Add the mapping information 3</td></tr>
<tr><td>msgType</td><td>Message Type</td></tr>
<tr><td>readYn</td><td>Whether reading</td></tr>
<tr><td>expireDate</td><td>Message validity date</td></tr>
<tr><td>regDate</td><td>Message Registration date</td></tr>
<tr><td>msgGrpCd</td><td>Message group code</td></tr>
</table>


## 3. Sample Code

### - InBox List Code
--
~~~java
private void setMsgList (Boolean state) {
	if (mMsgCursor != null) {
		mMsgCursor.close();
	}

	PMS pms = PMS.getInstance(context);
	// Select Msg List Limit 0, 50
	mMsgCursor = pms.selectMsgList(1, 50);
	
	// New Msg Count
	mNewMsgCountText.setText(String.format(mCon.getString(R.string.content_msg1), pms.selectNewMsgCnt()));

	if (state) {
		mAdapter = new MessageListAdapter(mCon, mMsgCursor, true);
		mMainListView.setAdapter(mAdapter);
	} else {
		mAdapter.swapCursor(mMsgCursor);
	}
}
~~~

### - InBox Row Date
~~~java
public void bindView (View view, Context context, Cursor cursor) {
	ViewHolder holder = (ViewHolder) view.getTag();

	Msg msg = new Msg(cursor); // Row Msg Class In Date

	holder.mTitleText.setText(msg.pushTitle); // Push Message Title
	holder.mContentText.setText(msg.pushMsg); // Push Message Content

	holder.mMainLayout.setTag(cursor.getPosition());
	holder.mMainLayout.setOnClickListener(rowClickListener);

	// Push Message Read Y & N
	if (msg.readYn.equals(FLAG_N)) {
		((RelativeLayout) view.findViewById(R.id.count_layout)).setVisibility(View.VISIBLE);
		holder.mCountText.setText(msg.readYn);
	} else {
		((RelativeLayout) view.findViewById(R.id.count_layout)).setVisibility(View.GONE);
	}

	// Push Message Type (T : Text Push, H & l : Rich Push)
	if (msg.msgType.equals("H") || msg.msgType.equals("l")) {
		holder.mTypeImgview.setVisibility(View.VISIBLE);
	} else {
		holder.mTypeImgview.setVisibility(View.GONE);
	}

	// Push Reg_date Convert (Default : yyyyMMddkkmmss)
	String date = DateUtil.convertDate(msg.regDate, "yyyyMMddkkmmss", "yyyy/MM/dd kk:mm");
	String time = date.substring(11);
	time = getTimeChange(time, "24", "00");
	holder.mDateText.setText(date.substring(0, 11) + time);
}
~~~

### - InBox Detail Date
~~~java
private void setMsgList () {
	// Importing the data into the appropriate user message ID value
	Msg msg = mPms.selectMsgWhereUserMsgId(mstrMsgUserId);

	// If you read ReadMsg call processing afterwards
	if (msg.readYn.equals(FLAG_N)) {
		JSONArray reads = new JSONArray();
		reads.put(msg.userMsgId);

		new ReadMsg(mCon).requestUserId(reads, new APICallback() {
			@Override
			public void response (String code, JSONObject json) {
				if (CODE_SUCCESS.equals(code)) {
					// List updated code
				}
			}
		});
	}

	//Branch processing according to the message type
	if (mstrMsgType.equals("T")) { 
		// Text Push
		String date = DateUtil.convertDate(msg.regDate, "yyyyMMddkkmmss", "yyyy/MM/dd kk:mm");
		mDateText.setText(date);
		mContentText.setText(msg.msgText);
	} else if (mstrMsgType.equals("H") || mstrMsgType.equals("l")) {
		// Rich Push
		webViewSetting(msg);
	}
}

private void webViewSetting (final Msg msg) {
	// WebView Setting
	mWebView.clearCache(true);
	mWebView.clearHistory();
	mWebView.clearFormData();

	mWebView.setInitialScale(1);
	mWebView.setBackgroundColor(android.R.style.Theme_Translucent);
	mWebView.setHorizontalScrollBarEnabled(false);
	mWebView.setVerticalScrollBarEnabled(false);

	WebSettings settings = mWebView.getSettings();
	settings.setJavaScriptEnabled(true);
	settings.setJavaScriptCanOpenWindowsAutomatically(true);
	settings.setSupportMultipleWindows(true);
	settings.setUseWideViewPort(true);
	settings.setLoadWithOverviewMode(true);
	settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
	settings.setGeolocationEnabled(true);
	settings.setDomStorageEnabled(true);
	settings.setUseWideViewPort(true);
	settings.setPluginState(PluginState.ON);

	mWebView.setWebChromeClient(new WebChromeClient() {
		@Override
		public void onProgressChanged (WebView view, int newProgress) {
			super.onProgressChanged(view, newProgress);
			mProgressbar.setProgress(newProgress);
			if (newProgress >= 100) {
				mProgressbar.setVisibility(View.GONE);
			}
		}
	});

	// Pms Click APi Call
	mWebView.addJavascriptInterface(new PMSWebViewBridge(mCon), "pms");

	if (Msg.TYPE_L.equals(msg.msgType) && msg.msgText.startsWith("http")) {
		mWebView.loadUrl(msg.msgText);
	} else {
		mWebView.loadDataWithBaseURL(null, msg.msgText, "text/html", "utf-8", null);
	}

	mWebView.setOnTouchListener(new OnTouchListener() {
		@Override
		public boolean onTouch (View v, MotionEvent event) {
			if (event.getAction() != MotionEvent.ACTION_MOVE) {
				((WebView) v).setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading (WebView view, String url) {
						CLog.i("webViewClient:url=" + url);
						Intent intent = new Intent(Intent.ACTION_VIEW);
						Uri uri = Uri.parse(url);
						intent.setData(uri);
						startActivity(intent);
						return true;
					}
				});
			}
			return false;
		}
	});

	mWebView.setWebViewClient(new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading (WebView view, String url) {
			CLog.i("webViewClient:url=" + url);
			return super.shouldOverrideUrlLoading(view, url);
		}
	});
}
~~~