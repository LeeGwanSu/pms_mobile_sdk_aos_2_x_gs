# PMS_SDK_Popup_UI_Guide



[TOC]

# 1. PMSPopup.class

## 기본 method

### public static PMSPopup getInstance(Context context)

---



* description
  - context를 파라미터로 하여 PMSPopup객체를 가져옵니다.

* return
  - PMSPopup

* parameter
  - Context context


---



### public void setPopUpBackColor(int red, int green, int blue, int alpha)

--

* description
  - PMS Push Popup Layout 전체 백그라운드 색상 & 투명도를 설정합니다.

* parameter 
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)
  - int alpha : 0 ~ 255 (255 : Max)


--
### - public void setPopupBackImgResource(String filename)
--

* description
  - PMS Push Popup Layout 전체 백그라운드 이미지를 설정합니다.

* parameter
  - String filename : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정)


--
### - public void setTopLayoutFlag(Boolean flag)
--

* description
  - PMS Push Popup Layout 중 상위 Title Bar를 사용 여부.

* parameter
  - Boolean flag : ON & OFF (true & false)


--
### - public void setTopBackColor(int red, int green, int blue, int alpha)
--

* description
  - PMS Push Popup Top Layout 백그라운드 색상 & 투명도를 설정합니다.

* parameter 
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)
  - int alpha : 0 ~ 255 (255 : Max)


--
### - public void setTopBackImgResource(String filename)
--

* description
  - PMS Push Popup Top Layout 백그라운드 이미지를 설정합니다.

* parameter
  - String filename : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정)


--
### - public void setTopTitleType(String type)
--

* description 
  - PMS Push Popup Top Layout 의 설정을 Image & text 인지 설정합니다.

* parameter
  - String type : 'text' & 'image'


--
### - public void setTopTitleImgResource(String filename)
--

* description
  - PMS Push Popup Top Title 을 이미지를 설정 합니다. (setTopTitleType을 'image'로 설정했을 경우)

* parameter
  - String filename : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정)


--
### - public void setTopTitleName(String name)
--

* description
  - PMS Push Popup Top Title 을 덱스트로 설정 합니다. (setTopTitleType을 'text'로 설정했을 경우)

* parameter
  - String name : Title 넣을 String name 값


--
### - public void setTopTitleTextColor(int red, int green, int blue)
--

* description
  - PMS Push Popup Top Title 에 대한 텍스트 색상을 설정 합니다.

* parameter
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)


--
### - public void setTopTitleTextSize(int size)
--

* description
  - PMS Push Popup Top Title 에 대한 텍스트 사이즈를 설정 합니다.

* parameter
  - int size : Title Name Text Size (Default : 25px)


--
### - public void setContentBackColor(int red, int green, int blue, int alpha)
--

* description
  - PMS Push Popup Top Layout 백그라운드 색상 & 투명도를 설정합니다.

* parameter 
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)
  - int alpha : 0 ~ 255 (255 : Max)


--
### - public void setContentBackImgResource(String filename)
--

* description
  - PMS Push Popup Content Layout 백그라운드 이미지를 설정합니다.

* parameter
  - String filename : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정)


--
### - public void setContentTextColor(int red, int green, int blue)
--

* description
  - PMS Push Popup Content Title 에 대한 텍스트 색상을 설정 합니다.

* parameter
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)


--
### - public void setContentTextSize(int size)
--

* description
  - PMS Push Popup Content 텍스트 사이즈를 설정 합니다.

* parameter
  - int size : Content Text Size (Default : 15px)


--
### - public void setBottomBackImgResource(String filename)
--

* description
  - PMS Push Popup Bottom Layout 백그라운드 이미지를 설정합니다.

* parameter
  - String filename : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정)


--
### - public void setBottomBackColor(int red, int green, int blue, int alpha)
--

* description
  - PMS Push Popup Bottom Layout 백그라운드 색상 & 투명도를 설정합니다.

* parameter 
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)
  - int alpha : 0 ~ 255 (255 : Max)


--
### - public void setBottomTextBtnCount(int count)
--

* description
  - Text Push Popup 에 표시할 버튼 갯수를 설정합니다.

* parameter
  - int count : 버튼 갯수 (1 ~ 2개)


--
### - public void setBottomRichBtnCount(int count)
--

* description
  - Rich Push Popup 에 표시할 버튼 갯수를 설정합니다.

* parameter
  - int count : 버튼 갯수 (1 ~ 2개)


--
### - public void setBottomBtnImageResource(String... args)
--

* description
  - Push Popup Bottom Button 에 적용할 이미지를 설정합니다.

* parameter
  - String... args[] : Image File Name (나인패치일 경우 `*.9.png` 중 `*` 값만 설정) 
  - ex) setBottomBtnImageResource("btn_1", "btn_2");


--
### - public void setBottomTextViewFlag(Boolean state)
--

* description
  - Push Popup Bottom Button 에 텍스트를 사용 할것인지 유 & 무

* parameter
  - Boolean state : ON & OFF (true & false)


--
### - public void setBottomBtnTextName(String... name)
--

* description
  - Push Popup Bottom Button Text 값 설정합니다.
  - setBottomTextBtnCount 값과 동일해야 합니다.

* parameter
  - String... name[] : 버튼 텍스트 네임 배열값
  - ex) setBottomBtnTextName("첫번째", "두번째");


--
### - public void setBottomBtnTextColor(int red, int green, int blue)
--

* description
  - Push Popup Bottom Button Text 값을 설정시 색상을 설정합니다.

* parameter
  - int red : 0 ~ 255 (255 : Max)
  - int green : 0 ~ 255 (255 : Max)
  - int blue : 0 ~ 255 (255 : Max)


--
### - public void setBottomBtnTextSize(int size)
--

* description
  - Push Popup Bottom Button Text 값을 설정시 글자 크기을 설정합니다.

* parameter
  - int size : Button Text Size (Default : 15px)


--
### - public void setTextBottomBtnClickListener(Object... ocl)
--

* description
  - Text Push Popup 시 버튼에 대한 이벤트를 등록 합니다.

* parameter
  - Object... ocl[] : OnClickListener 를 implements 한 클래스 객체

* example 
  - 버튼 이벤트 클래스 기본형 (필수)
    <pre>
    class myClick implements OnClickListener, Serializable {
    	private static final long serialVersionUID = 1L;

    	@Override
    	public void onClick(View v) {
    		// Add User Code
    		getActivity().finish();
    	}
    }
    </pre>

  - 등록시
    <pre>
    setTextBottomBtnClickListener(new myClick1(), new myClick2());
    </pre>


--
### - public void setRichBottomBtnClickListener(Object... ocl)
--

* description
  - Rich Push Popup 시 버튼에 대한 이벤트를 등록 합니다.

* parameter
  - Object... ocl[] : OnClickListener 를 implements 한 클래스 객체

* example 
  - 버튼 이벤트 클래스 기본형 (필수)
    <pre>
    class myClick implements OnClickListener, Serializable {
    	private static final long serialVersionUID = 1L;

    	@Override
    	public void onClick(View v) {
    		// Add User Code
    		getActivity().finish();
    	}
    }
    </pre>

  - 등록시
    <pre>
    setTextBottomBtnClickListener(new myClick1(), new myClick2());
    </pre>


--
### - public void setLayoutXMLTextResId (String filename)
--

* description
  - Text Push Popup 을 작성한 Layout XML을 설정합니다.

* parameter
  - String filename : Text Popup Layout XML 파일 이름


--
### - public void setLayoutXMLRichResId (String filename)
--

* description
  - Text Push Popup 을 작성한 Layout XML을 설정합니다.

* parameter
  - String filename : Text Popup Layout XML 파일 이름


--
### - public void setLayoutXMLRichResId (String filename)
--

* description
  - Text Push Popup 을 작성한 Layout XML을 설정합니다.

* parameter
  - String filename : Text Popup Layout XML 파일 이름


--
### - public void setXMLTextButtonType (String...viewtype)
--

* description
  - Text Popup 창 버튼 리소스 종류 
  - ex) "ImageView", "Button"

* parameter
  - String...viewtype : 버튼 리소스 배열값
  - ex) setXMLTextButtonType ("button", "ImageView")


--
### - public void setXMLTextButtonTagName (String...name)
--

* description
  - Text Popup 창 버튼의 Tag Name
  - ex) "button1", "button2"

* parameter
  - String...name : Tag Name 배열값
  - ex) setXMLTextButtonTagName ("button1", "button2")


--
### - public void setXMLRichButtonType (String...viewtype)
--

* description
  - Rich Popup 창 버튼 리소스 종류 
  - ex) "ImageView", "Button"

* parameter
  - String...viewtype : 버튼 리소스 배열값
  - ex) setXMLRichButtonType ("button", "ImageView")


--
### - public void setXMLRichButtonTagName (String...name)
--

* description
  - Rich Popup 창 버튼의 Tag Name
  - ex) "button1", "button2"

* parameter
  - String...name : Tag Name 배열값
  - ex) setXMLRichButtonTagName ("button1", "button2")


--
### - public Activity getActivity ()
--

* description
  - Push Popup 에 대한 Activity 값을 가져 옵니다.

* return
  - Push Popup Activity 값


--
### - public void commit()
--

* description
  - Push Popup 에 설정한 값들을 저장합니다.
  - 설정이 끝난후 꼭 넣어 주셔야 합니다.


--
### - public void setDefaultPopup (String titlename)
--

* description
  - Push Popup 에 대한 Default UI를 자동으로 설정합니다.

* parameter
  - String titlename : Push Popup Title 에 등록될 네임값


--
### - public void startNotiReceiver()
--

* description
  - setNotiReceiver() 등록된 클래스를 실행하는 Methed()



# 2. Push Popup UI 구조

## UI 상세구조

### - Image 삽입 구조
![enter image description here][1]


[1]: https://lh3.googleusercontent.com/-VNInF3N4e08/UrKq4BaQJzI/AAAAAAAABek/ms32lf1mM3s/s0/pushpopup1.jpg "pushpopup1.jpg"

  

### Default 기본 항목

---

* 출하는 곳 예제

  ```
   PMS.setPopupSetting(true, "HUNUSON"); // PMS Class 참조
  ```

  ​

* setDefault 구성요소
  ```java
  public void setDefaultPopup (String titlename) {
    setPopUpBackColor(128, 128, 128, 200);
    setTopLayoutFlag(true);
    setTopTitleType("text");
    setTopTitleTextColor(255, 255, 255);
    setTopTitleName(titlename);
    setContentTextColor(255, 255, 255);
    setBottomTextBtnCount(2);
    setBottomRichBtnCount(1);
    setBottomTextViewFlag(false);
    setBottomBtnTextName("닫  기", "자세히 보기");
    setTextBottomBtnClickListener(new myClick(), new myClick1());
    setRichBottomBtnClickListener(new myClick());
  }
  ```

  ​

  ​
### Image 적용 요소

```java
pmspopup.setXmlAndDefaultFlag(false);
// Popup Layout Setting 
pmspopup.setPopupBackImgResource("pms_bg_popup");
	
// Top Layout Setting
pmspopup.setTopLayoutFlag(true);
pmspopup.setTopBackColor(56, 98, 196, 255);
pmspopup.setTopTitleType("image");
pmspopup.setTopTitleImgResource("pms_img_logo");
```


```java
// Content Layout Setting
pmspopup.setContentBackColor(255, 255, 255, 255);
pmspopup.setContentTextColor(0, 0, 0);

// Bottom Button Layout Setting
pmspopup.setBottomTextBtnCount(2);
pmspopup.setBottomRichBtnCount(1);
pmspopup.setBottomBackColor(224, 224, 224, 255);

pmspopup.setBottomTextViewFlag(true);
pmspopup.setBottomTextBtnCount(2);
pmspopup.setBottomRichBtnCount(1);
pmspopup.setBottomBtnTextName("닫  기", "자세히 보기");
pmspopup.setBottomBtnTextColor(255, 255, 255);
pmspopup.setBottomBtnImageResource("pms_btn_text_close_off", "pms_btn_text_detail_off");
pmspopup.setTextBottomBtnClickListener(new myClick(pmspopup), new myClick1(pmspopup));
pmspopup.setRichBottomBtnClickListener(new myClick(pmspopup));

pmspopup.commit();
```



### XML 기본 구조 (PMS_TEXT_POPUP시)

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:gravity="center">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="#f0f0f0"
        android:orientation="vertical" >

        <TextView 
        	android:layout_width="match_parent"
        	android:layout_height="match_parent"
        	android:layout_gravity="center"
        	android:gravity="center"
            android:background="#FF3862C4"
            android:padding="10dp"
            android:text="ASP"/>

        <ScrollView
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1.0"
            android:background="#FFFFFFFF" >

            <TextView
                android:id="@+id/pms_txt_msg"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:tag="ContentTextView"
                android:padding="10dp"
                android:textColor="#FF222A3B" />
        </ScrollView>

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="bottom"
            android:background="#FFE0E0E0"
            android:gravity="center"
            android:orientation="horizontal" >

            <LinearLayout
                android:id="@+id/pms_btn_close"
                android:layout_width="100dp"
                android:layout_height="wrap_content"
                android:layout_margin="10dp"
                android:background="#f3f3f3"
                android:clickable="true"
                android:gravity="center"
                android:orientation="horizontal"
                android:padding="10dp" >

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center_vertical"
                    android:layout_marginLeft="10dp"
                    android:tag="button1"
                    android:text="닫기"
                    android:textColor="#FF000000" />
            </LinearLayout>

            <LinearLayout
                android:id="@+id/pms_btn_detail"
                android:layout_width="100dp"
                android:layout_height="wrap_content"
                android:layout_margin="10dp"
                android:background="#f3f3f3"
                android:clickable="true"
                android:gravity="center"
                android:padding="10dp" >

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:tag="button2"
                    android:text="자세히 보기"
                    android:textColor="#FF000000" />
            </LinearLayout>
        </LinearLayout>
    </LinearLayout>
</RelativeLayout>
```


* Content Text 가 쓰이는 부분은 꼭 TextView 를 이용하여 Tag 를 'ContentTextView' 삽입
* Button이 되는 Resource Type은 android:tag 값을 지정.
* setXMLTextButtonType & setXMLTextButtonTagName 등록 해주셔야 합니다.




### XML 기본 구조 (PMS_RICH_POPUP시)

```xml
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:background="#f8f8f8"
    android:gravity="center" >

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingBottom="20dp"
        android:gravity="center" >

        <ImageView
            android:id="@+id/pms_img"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="3dp"
            android:scaleType="centerCrop" />

        <WebView
            android:id="@+id/pms_wv"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:tag="ContentWebView"
            android:layout_margin="3dp" />

        <ProgressBar
            android:id="@+id/pms_prg"
            style="?android:attr/progressBarStyleHorizontal"
            android:tag="ContentProgressBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center_horizontal"
            android:layout_margin="4dp" />
    </RelativeLayout>

    <LinearLayout
         android:id="@+id/pms_btn_close"
         android:layout_width="100dp"
         android:layout_height="wrap_content"
         android:layout_margin="10dp"
         android:background="#f3f3f3"
         android:clickable="true"
         android:gravity="center"
         android:orientation="horizontal"
         android:padding="10dp" >

         <TextView
             android:layout_width="wrap_content"
             android:layout_height="wrap_content"
             android:layout_gravity="center_vertical"
             android:layout_marginLeft="10dp"
             android:tag="button1"
             android:text="닫기"
             android:textColor="#FF000000" />
     </LinearLayout>
</FrameLayout>
```


* Content Webview 가 쓰이는 부분은 꼭 WebView 를 이용하여 Tag 를 'ContentWebView' 삽입
* Content Webview 중 ProgressBar는 Tag 를 'ContentProgressBar' 삽입
* Button이 되는 Resource Type은 android:tag 값을 지정.
* setXMLRichButtonType & setXMLRichButtonTagName 등록 해주셔야 합니다.




### XML 삽입 구조

```java
pmspopup.setXmlAndDefaultFlag(true);
pmsPopup.setLayoutXMLTextResId("pms_text_popup");
pmsPopup.setXMLTextButtonType("TextView", "TextView");
pmsPopup.setXMLTextButtonTagName("button1", "button2");
		
pmsPopup.setLayoutXMLRichResId("pms_rich_popup");
pmsPopup.setXMLRichButtonType("TextView");
pmsPopup.setXMLRichButtonTagName("button1");
	
pmspopup.setTextBottomBtnClickListener(new myClick(pmspopup), new myClick1(pmspopup));
pmspopup.setRichBottomBtnClickListener(new myClick(pmspopup));
	
pmspopup.commit();
```


## **필수 사항**
* UI 라이브러리 적용시 적용되는 class는 물론 상위에서 호출되는 class들 마다 'implements Serializable' 를 해줘야 한다
