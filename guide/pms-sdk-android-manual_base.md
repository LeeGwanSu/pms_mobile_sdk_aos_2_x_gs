# PMS Manual

[TOC]



# 1. xml 파일 설정

## 1.1. AndroidManifest.xml 설정


**PMS수행을 위한 Permission, Service, Receiver, Meta Data등을 설정합니다.**

**AndroidManifest.xml 참조**



### permission 추가

```xml
<!-- push -->
<permission android:name="${project_package}.permission.C2D_MESSAGE" android:protectionLevel="signature" />
    
<uses-permission android:name="${project_package}.permission.C2D_MESSAGE" />
<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
<uses-permission android:name="android.permission.USE_CREDENTIALS" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
<uses-permission android:name="android.permission.GET_TASKS" />
<uses-permission android:name="android.permission.DISABLE_KEYGUARD"/>
<uses-permission android:name="android.permission.VIBRATE" />
<!-- push -->
    
<!-- network -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<!-- network -->
    
<!-- storage -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<!-- storage -->
    
<!-- state -->
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<!-- state -->
```

* `<manifest>` 태그내에 추가합니다.
* `${project_package}`는 manifest태그의 package를 의미합니다.


### app key 및 server url 추가

```xml
<!-- PMS APP KEY -->
<meta-data android:name="PMS_APP_KEY" android:value="\ ${app_key}" />

<!-- PMS SERVER URL -->
<meta-data android:name="PMS_API_SERVER_URL" android:value="${server_url}" />
```
* `<application>` 태그내에 추가합니다.
* `${app_key}`는 pms admin web에서 앱 생성 시 발급되는 key입니다.
* `${server_url}`은 pms msg-api url을 의미합니다.


### GCM receiver 추가

```xml
<!-- pms GCM Receiver -->
<receiver
	android:name="com.pms.sdk.push.PushReceiver"
	android:permission="com.google.android.c2dm.permission.SEND" >
	<intent-filter>
		<action android:name="com.google.android.c2dm.intent.RECEIVE" />
		<action android:name="com.google.android.c2dm.intent.REGISTRATION" />
		<category android:name="${project_package}" />
	</intent-filter>
</receiver>
```
* `<application>` 태그내에 추가합니다.
* `${project_package}`는 manifest태그의 package를 의미합니다.


### PushPopupActivity 추가

```xml
<!-- pms push popup activity -->
<activity
	android:name="${push_popup_activity}"
	android:theme="@style/push_popup_theme"
	android:configChanges="orientation" />
```
*  `<application>` 태그내에 추가합니다.
*  `${push_popup_activity}`는 Push수신 시 출력될 Popup Activity의 className을 의미합니다.
* PMS에서 제공되는 기본 PushPopupActivity를 쓰고자 한다면, "com.pms.sdk.push.PushPopupActivity"를 셋팅하시면 됩니다.
* Custom PushPopupActivity를 쓰고자 한다면, 개발하신 className을 `${push_popup_activity}`에 셋팅하시고, PMS클래스의 `setPushPopupActivity(String)`을 호출해주시면 됩니다.
* Custom PushPopupActivity 적용 예제 (PushPopupAcitivity className이 "com.custom.push.CusomPushPopupActivity"일 경우)
  ```xml
  <!-- pms push popup activity -->
  <activity
  android:name="com.custom.push.CusomPushPopupActivity"
  android:theme="@style/push_popup_theme"
  android:configChanges="orientation" />
  ```

  ```java
  PMS.getInstance(context).setPushPopupactivity("com.custom.push.CusomPushPopupActivity");
  ```

  ​
  ​


### PushNotiReceiver 추가

```xml
<!-- pms push clickNotiReceiver -->
<receiver android:name="${noti_receiver_class}" >
	<intent-filter>
		<action android:name="${noti_receiver}" />
	</intent-filter>
</receiver>
```
* `<application>` 태그내에 추가합니다.
* `${noti_receiver_class}`는 Push수신 시 출력되는 상단의 Notification을 터치 했을 때, 수행될 Receiver의 className을 의미합니다.
* `${noti_receiver}`는 Push수신 시 출력되는 상단의 Notification을 터치 했을 때, broadcasting할 intent action을 의미합니다.
* java에서 `setNotiRceiver(String)`메소드를 이용하여 noti receiver(intent action)를 셋팅해주셔야 합니다.
* NotiReceiver 적용 예제 (NotiReceiverClass className이 "com.custom.push.CustomNotiReceiverClass"이고, notiReceiver intent action이 "com.custom.push.notifiaction"일 경우)
  ```xml
  <!-- pms push clickNotiReceiver -->
  <receiver android:name="com.custom.push.CustomNotiReceiverClass" >
  	<intent-filter>
  		<action android:name="com.custom.push.notifiaction" />
  	</intent-filter>
  </receiver>
  ```

  ```java
  PMS.getInstance(context).setNotiReceiver("com.custom.push.notifiaction");
  ```

  ​
  ​

### MQTT(Pirvate Server) service, receiver 및 meta-data 추가

```xml
<!-- pms MQTT RestartReceiver -->
<receiver android:name="com.pms.sdk.push.mqtt.RestartReceiver">
	<intent-filter>
		<action android:name="ACTION_MQTT_PING" />
		<action android:name="android.intent.action.BOOT_COMPLETED" />
		<action android:name="android.intent.action.USER_PRESENT" />
		<action android:name="android.intent.action.ACTION_PACKAGE_RESTARTED" />
	</intent-filter>
</receiver>
    
<!-- pms MQTT connectionChangeReceiver -->
<receiver android:name="com.pms.sdk.push.mqtt.ConnectionChangeReceiver" android:label="NetworkConnection">
	<intent-filter>
		<action android:name="android.net.conn.CONNECTIVITY_CHANGE"/>
	</intent-filter>
</receiver>
    
<!-- Private PUSH Service -->
<service
	android:name="com.pms.sdk.push.mqtt.MQTTService"
	android:enabled="true"
	android:exported="true"
	android:label="PushService"
	android:process="com.pms.sdk.HumusonpushServicec" />
    
<!-- Private PUSH Receiver -->
<receiver android:name="com.pms.sdk.push.PushReceiver" >
	<intent-filter>
		<action android:name="org.mosquitto.android.mqtt.MSGRECVD" />
		<category android:name="${project_package}" />
	</intent-filter>
</receiver>
    
<!-- PMS MQTT FLAG -->
<meta-data android:name="PMS_MQTT_FLAG" android:value="${mqtt_flag}" />
    
<!-- PMS MQTT SERVER URL -->
<meta-data android:name="PMS_MQTT_SERVER_URL_SSL" android:value="${mqtt_ssl_server_url}" />
<meta-data android:name="PMS_MQTT_SERVER_URL_TCP" android:value="${mqtt_tcp_server_url}" />
    
<!-- PMS MQTT SERVER KEEPALIVE (second) -->
<meta-data android:name="PMS_MQTT_SERVER_KEEPALIVE" android:value="${mqtt_server_keepalive}" />
```
* `<application>` 태그내에 추가합니다.

* `${project_package}`는 manifest태그의 package를 의미합니다.

* `${mqtt_flag}`는 mqtt의 사용여부를 의미합니다. ("Y":사용, "N":사용하지 않음)

* private server와 연동을 하지 않는다면 다음과 같이 추가 해주시면 됩니다.

* `<service>` 태그내에 process 내용은 원하시는 명칭으로 바꾸셔도 상관은 없습니다.

  ```xml
  <!-- PMS MQTT FLAG -->
  <meta-data android:name="PMS_MQTT_FLAG" android:value="N" />
  ```

  ​


* ${mqtt_server_url}`은 private server와 연동 시 필요한 server url을 의미합니다. 반드시 다음과 같이 protocol, url, port를 전부 입력해주셔야 합니다.

  ```xml
  <!-- PMS MQTT SERVER URL -->
  <meta-data android:name="PMS_MQTT_SERVER_URL_SSL" android:value="ssl://pms.push.com:1883" />
  <meta-data android:name="PMS_MQTT_SERVER_URL_TCP" android:value="tcp://pms.push.com:1883" />
  ```

* `${mqtt_server_keepalive}`는 private server와의 keep alive time을 의미합니다. 단위는 초(second)입니다.




### Notification 설정 값 추가

```xml
<meta-data android:name="PMS_NOTI_CONTENT" android:value="두손가락을 이용해 아래로 당겨주세요." />
```
* Image 포함한 Notification 노출시 Image가 안보일시 보이는 메세지 내용입니다.
* 위의 값을 설정해주면 위의 내용이 보이게 됩니다.




###Notification Icon 설정 값 추가

```xml
<meta-data android:name="PMS_SET_ICON" android:resource="@drawable/${icon file name}" />
```
* Notification Icon에 등록될 Icon File Name을 넣으시면 됩니다.



### style 추가

**style.xml에 push popup의 투명배경을 위한 style을 추가해줘야 합니다.**
```xml
<!-- push popup theme -->
<style name="push_popup_theme" parent="android:Theme.Light">
	<item name="android:windowNoTitle">true</item>
	<item name="android:windowIsTranslucent">true</item>
	<item name="android:windowBackground">@android:color/transparent</item>
	<item name="android:windowContentOverlay">@null</item>
</style>
```



# 2. PMS.class

## 2.1. 필수 method

### public static PMS getInstance(Context context, String projectId)

---

**context와 projectId를 파라미터로 하여 PMS객체를 가져옵니다.**

**projectId는 GCM발송을 위한 앱의 projectId로 GCM의 pushToken을 가져오는데 사용됩니다.**

* return
  - PMS

* parameter
  - Context context
  - String projectId

* 참고
  - http://code.google.com/apis/console에 접속하여 구글 아이디로 로그인을 하면, 웹브라우저의 URL이 
    https://code.google.com/apis/console/#project:123456789012와 같이, ‘#project:’ 뒤쪽에 
    숫자(123456789012)가 출력됩니다. 이 숫자는 프로젝트 ID로 GCM(google Cloud Messaging) 을 사용하기 위한 
    token을 받을 때 사용됩니다.
  - Reference : http://developer.android.com/guide/google/gcm/gs.html


---


### public static PMS getInstance(Context context)

---
**context를 파라미터로 하여 PMS객체를 가져옵니다.**

* return
  - PMS

* parameter
  - Context context


---


### public static PMSPopup getPopUpInstance()

---

**context를 파라미터로 하여 PMSPopup객체를 가져옵니다.**

* return
  - PMSPopup

* parameter
  - Context context


---


### public static boolean clear()

---

**PMS Instance를 메모리에서 제거합니다. 앱이 종료되는 시점에 호출해줘야 합니다.**

* return
  - boolean


---


### public void setCustId(String custId)

---
**로그인 시 입력되는 사용자 아이디를 설정합니다.**

* parameter
  - String custId


---


### public void setIsPopupActivity(Boolean ispopup)

---
**다른앱을 사용시에 팝업을 노출 시킬것인지 말것인지 설정합니다.**

* parameter
  - Boolean ispopup -> true : 노출안시킴 & false : 노출시킴


---

### public void setPopupSetting (Boolean state, String title)

---
**PMSPopup 객체를 생성하고 Default Popup을 설정합니다.**

* parameter
  - Boolean state true: Default 사용, false: 사용안함.
  - String title 팝업 타이틀에 표시될 String


---

## 2.2. 추가 method

### public void setDebugTAG(String tagName)

---
**로그(logcat) TAG를 설정합니다.**

* parameter
  - String tagName


---
### public void setDebugMode(boolean debugMode)

---
**로그(logcat) 출력여부를 설정합니다.**

* parameter
  - boolean debugMode


---

## 2.3. Push 관련 method

### public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener)

---


**Push수신 시 수행되는 리스터 입니다.**

**파라미터인 onReceivePushListener의 리턴형태에 따라 Notification Bar를 출력합니다.**

**return값이 true일 경우 Notifcation Bar를 출력합니다.**

**return값이 false일 경우 Notifcation Bar를 출력하지 않습니다.**

* parameter
  - OnReceiverPushListener onReceivePushListener

* example
  ```java
  PMS pms = PMS.getInstance(getApplicationContext());
  pms.setOnReceivePushListener(new OnReceivePushListener() {
  @Override
  public boolean onReceive(Context context, Bundle bundle) {

  	if (getAppVisable()) {
  	// App이 올라와있는 상태라면,
  		// notificatoin bar를 출력하지 않는다.
  		return false;
  	} else {
  	// App이 올라와있지 않다면,
  		// notification bar를 출력한다.
  		return true;
  		}
  }
  	
  });
  ```



---



### public String getMsgFlag()

---


**Push Msg수신 설정 정보를 가져옵니다.**

* return
  - "Y" or "N"


---


### public String getNotiFlag()

---
**Push Noti노출 설정 정보를 가져옵니다.**

* return
  - "Y" or "N"


---


### public void setLargeNotiIcon(int resId)

---
**Push 수신 시 Notification Bar에 출력될 큰 사이즈의 아이콘의 resource id를 설정합니다.**

* parameter
  - int resId


---


### public void setNotiSound(int resId)

---
**Push 수신 시 출력될 사운드를 설정합니다.**

* parameter
  - int resId


---


### public void setNotiReceiver(String intentAction)

---
**Notification Bar 터치 시 받게 될 receiver 클래스를 설정합니다.**

* parameter
  - String intentAction


---


### public void setRingMode(boolean isRingMode)

---
**Push 수신 시 ring여부를 설정합니다.**

* parameter
  - boolean isRingMode


---


### public void setVibeMode(boolean isVibeMode)

---
**Push 수신 시 vibe여부를 설정합니다.**

* parameter
  - boolean isVibeMode


---


### public void setPopupNoti(boolean isShowPopup)

---
**Push 수신 시 popup여부를 설정합니다.**

* parameter
  - boolean isShowPopup


---


### public void setPushPopupActivity(String classPath)

---
**푸시 수신 시, 출력될 popup activity를 설정합니다.**

**default activity : com.pms.sdk.push.PushPopupActivity**

* parameter
  - String classPath


---


### public void setPushPopupShowingTime(int pushPopupShowingTime)

---
**푸시 수신 시, 출력될 popup activity의 유지 시간을 설정합니다. (milisecond)**

* parameter
  - int pushPopupShowingTime


---

## 2.4. Message 관련 method

### public void bindBadge(Context c, int id)

---
**새로운 메시지 숫자를 셋팅할 뷰를 바인딩 합니다. badge(TextView)가 속한 context와 badge(TextView)의 
id를 매개변수를 넘겨줘야 합니다.**

**bindBadge()는 Activity의 onResume()에 작성되어야 합니다.**

* parameter
  - Context context
  - int id


---


### public void bindBadge(TextView badge)

---
**새로운 메시지 숫자를 셋팅할 뷰를 바인딩 합니다. badge(TextView)를 매개변수로 넘겨줘야 합니다.**
**bindBadge()는 Activity의 onResume()에 작성되어야 합니다.**

* parameter
  - TextView badge


---


### public void showMsgBox(Context c)

---
**InBox로 이동합니다.**

* parameter
  - Context c


---


### public void showMsgBox(Context c, Bundle extras)

---
**InBox로 이동합니다.**

* parameter
  - Context c
  - Bundle extras


---


### public void closeMsgBox(Context c)

---
**InBox를 종료합니다.**

* parameter
  - Context c


---

## 2.5. Private Push Server 관련 (MQTT) method

### public void startMQTTService(Context context)

---
**Private Push Server Service를 실행합니다.**

* parameter
  - Context context


---


### public void stopMQTTService(Context context)

---
**Private Push Server Service를 중지합니다.**

* parameter
  - Context context


---

## 2.6. Database(SQLite) 관련 method

**SDK에서 사용하는 SQLite에 접근 하는 메소드들을 설명합니다.**

---


### public Cursor selectMsgGrpList()

---
**메시지 그룹 리스트를 가져옵니다.**

* return
  - Cursor


---


### public MsgGrp selectMsGrp(String msgCode)

---
**메세지 그룹을 가져옵니다.**

* parameter
  - String msgCode

* return
  - MsgGrp


---


### public int selectNewMsgCnt()

---
**새로운 메시지 수를 가져옵니다.**

* return
  - int


---


### public Cursor selectMsgList (int page, int row)

---
**메시지 리스트를 가져옵니다.**

* parameter
   - int page
   - int row

* return
   - Cursor


---


### public Cursor selectMsgList(String msgCode)

---
**메시지 리스트를 가져옵니다.**

* parameter
  - String msgCode

* return
  - Cursor


---


### public Msg selectMsgWhereMsgId(String msgId)

---
**메시지를 가져옵니다.**

* parameter
  - String msgId

* return
  - Msg


---


### public Msg selectMsgWhereUserMsgId(String userMsgId)

---
**메시지를 가져옵니다.**

* parameter
  - String userMsgId

* return
  - Msg


---


### public long updateMsgGrp(String msgCode, ContentValues values)

---
**메시지 그룹을 업데이트 합니다.**

* parameter
  - String msgCode
  - ContentValues values

* return
  - long


---


### public long public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId)

---
**메시지를 읽음으로 갱신합니다.**

* parameter
  - String msgGrpCd
  - String firstUserMsgId
  - String lastUserMsgId

* return
  - long


---


### public long updateReadMsgWhereUserMsgId (String userMsgId)

---
**메시지를 읽음으로 갱신합니다.**

* parameter
  - String userMsgId

* return
  - long


---


### public long updateReadMsgWhereMsgId (String msgId)

---
**메시지를 읽음으로 갱신합니다.**

* parameter
  - String msgId

* return
  - long


---


### public long deleteUserMsgId (String userMsgId)

---
**메시지를 삭제합니다.**

* parameter
  - String userMsgId

* return
  - long


---


### public long deleteMsgId (String MsgId)

---
**메시지를 삭제합니다.**

* parameter
  - String msgId

* return
  - long


---


### public long deleteMsgGrp(String msgCode)

---
**메시지 그룹을 삭제합니다.**

* parameter
  - String msgCode

* return
  - long


---

### public long deleteExpireMsg ()

---
**유효기간이 지난 메시지를 삭제합니다.**

---

### public void deleteEmptyMsgGrp()

---
**비어있는 메시지 그룹을 삭제 합니다.**

---

### public void deleteAll()

---
**모든 데이터를 삭제합니다.**

---



## 2.7. Push Data

**Push Data는 setOnReceivePushListener 메소드의 파라미터, onReceivePushListener에서 참조 할 수 있습니다.**

**onReceivePushListener의 intent에서 참조 할 수 있으며, 다음과 같이 데이터를 가져 올 수 있습니다.**

```java
intent.getStringExtra(PMS.KEY_MSG_ID) // "i" - 메세지 ID
intent.getStringExtra(PMS.KEY_NOTI_TITLE) // "notiTitle" - notification에 출력될 타이틀
intent.getStringExtra(PMS.KEY_NOTI_MSG) // "notiMsg" - notification에 출력될 메시지 내용
intent.getStringExtra(PMS.KEY_NOTI_IMG) // "notiImg" - notification에 출력될 이미지 URL
intent.getStringExtra(PMS.KEY_MSG) // "message" - (리치) 푸시 내용
intent.getStringExtra(PMS.KEY_SOUND) // "sound" - 푸시 수신 시 출력될 사운드
intent.getStringExtra(PMS.KEY_MSG_TYPE) // "t" - 메시지 타입 : H – html, T – Text, L – Link
intent.getStringExtra(PMS.KEY_DATA) // "d" - 추가 데이터
```

## 2.8. Notification Bar Touch에 대한 BroadcastReceiver 작성

**Push수신 시 출력되는 Notification Bar의 터치에 대한 특정 로직을 삽입하고자 할 때, 작성하는 class 입니다.**

**NotiReceiver 셋팅은 `pushNotiReceiver 추가`항목을 참조 하시기 바랍니다.**

### CustomNotiReceiverClass

* BroadcaseReceiver를 extends받아서 작성 합니다.

* intent에서 Data를 가져오는 방법은 `Push Data`항목을 참조 하시기 바랍니다.

  ```java
  package com.custom.push;

  import android.content.BroadcastReceiver;
  import android.content.Context;
  import android.content.Intent;

  public class CustomNotiReceiverClass extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // notification 터치 시에 “push notiMsg:${notiMsg}” 라는 텍스트를 Toast로 출력합니다.
        Toast.makeText(contetx, "push notiMsg:" + intent.getStringExtra(PMS.KEY_NOTI_MSG),
            Toast.LENGTH_SHORT).show();
    }
  }
  ```

  ​


## 2.9. Msg.class (bean)

**모든 푸시 메시지는 Message DB(SQLite)에 저장됩니다.**

**Msg class는 bean형태로 Message DB의 데이터가 저장됩니다.**



**Msg class는 다음과 같이 사용하실 수 있습니다.**

```java
Cusror c = PMS.getInstance(context).selectMsgList(1, 1000);
c.moveToPosition(10);
Msg msg = new Msg(c);
System.out.println(msg.id);
```



**Msg class의 변수는 다음과 같습니다.**

| 변수         | 설명                      |
| ---------- | ----------------------- |
| id         | DB의  row id             |
| userMsgId  | (서버에서의) 메시지의 unique한 ID |
| msgGrpName | 메시지의 그룹명                |
| appLink    | appLink(추가데이터)          |
| iconName   | iconName(추가데이터)         |
| msgId      | 메시지 ID                  |
| pushTitle  | push 타이틀                |
| pushMsg    | push 메시지                |
| msgText    | 메시지 텍스트(앱내 메세지)         |
| map1       | 추가매핑정보1                 |
| map2       | 추가매핑정보2                 |
| map3       | 추가매핑정보3                 |
| msgType    | 메시지 유형                  |
| readYn     | 읽음여부                    |
| expireDate | 메시지 유효 날짜               |
| regDate    | 메시지 등록날짜                |
| msgGrpCd   | 메시지 그룹코드                |





# 3. msg-api 호출 (com.pms.sdk.api.request)

**기본적인 msg-api 호출클래스의 사용방법은 다음과 같습니다.**

* callback이 필요한 경우

  ```java
  new ApiClass(context).request(${parameters}, new APICallback() {
    public void response(String code, JSONObject json) {
        ${callback_process}
    }
  });
  ```

  ​

* callback이 필요 없을 경우

  ```java
  new ApiClass(context).request(${parameters}, null);
  ```

  ​


## 3.1. DeviceCert.class

**디바이스의 기본 정보 및 유저 정보를 DB에 저장합니다.**

* request method
  - userData

    ```java
    public void request(final JSONObject userData, final APICallback apiCallback)
    ```

  - userData를 사용하지 않음

    ```java
    new DeviceCert(context).request(null, ${callback});
    ```

    ​

* userData는 사용자 정보로 다음과 같이 JSONObject로 작성해야 합니다.

  ```java
  JSONObject userData = new JSONObject();
  userData.put("custName", ${user_name});
  userData.put("phoneNumber", ${phone_number});
  userData.put("birthday", ${birthday});
  userData.put("location1", ${location1});
  userData.put("location2", ${location2});
  userData.put("gender", ${gender});
  userData.put("data1", ${data1});
  userData.put("data2", ${data2});
  userData.put("data3", ${data3});
  ```

  ​
  ​


## 3.2. LoginPms.class

**로그인(cust_id저장)을 수행합니다.**

* request method
  - custId
  - userData

  ```java
  public void request(String custId, final JSONObject userData, final APICallback apiCallback)
  ```

  ​
  ​

* DeviceCert와 마찬가지로 사용자 정보(userData) 셋팅이 가능하며, 셋팅하지 않으신다면 다음과 같이 호출하시면 됩니다.

  ```java
  new LoginPms(context).request(${custId}, null, ${callback});
  ```

  ​

## 3.3. NewMsg.class

**메시지를 가져옵니다.**

* request
  - type : ‘P’ (이전prev), ‘N’ (이후next)
  - reqUserMsgId : serMsgId이전 또는 이후의 userMsgId를 갖는 메시지들을 가져온다. (-1 일 경우는 새로운 메시지만 가져온다)
  - msgGrpCode : -1일 경우는 모든 msgGrpCd를 가져온다
  - pageNum : 페이지
  - pageSize : 로우

  ```java
  public void request(String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize, final APICallback apiCallback)
  ```

  ​

## 3.4. ReadMsg.class

**메시지를 읽음 처리 합니다.**

* request
  - reads : JSONObject로 만들어서 저장함. ex) {"msgId":"186463","workday":"20140602095749"}
  - PMSUtil.getReadParam(String msgId)를 이용해서 JSONObject 형식을 만듭니다.

  ```java
  public void request(JSONArray reads, final APICallback apiCallback)
  ```

  ​


## 3.5. SetConfig.class

**설정 정보를 저장합니다.**

* request
  - msgFlag : "Y" & "N"
  - notiFlag : "Y" & "N"

  ```java
  public void request(String msgFlag, String notiFlag, final APICallback apiCallback)
  ```

  ​

## 3.6. LogoutPms.class

**로그아웃을 수행합니다.**

* request

  ```java
  public void request(final APICallback apiCallback)
  ```

  ​
