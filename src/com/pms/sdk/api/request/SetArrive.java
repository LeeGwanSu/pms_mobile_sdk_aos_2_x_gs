package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.api.APIManager;
import com.pms.sdk.common.util.CLog;

import org.json.JSONObject;

/**
 * Created by humuson_sijoon on 2017. 9. 14..
 */

public class SetArrive extends BaseRequest {
/*
    private static int NUM_CHECK_ARRIVE_CNT = 3;
    private static int NUM_CHECK_ARRIVE_DELAY = 1000;

    private int mCheckNum = 0;
*/
    public SetArrive(Context context) {
        super(context);
    }


    /**
     * get param
     *
     * @param schdlId
     * @param sendRowId
     * @return
     */
    public JSONObject getParam(int schdlId, long sendRowId) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            jobj.put("schdlId", schdlId);
            jobj.put("sendRowId", sendRowId);
            return jobj;
        } catch (Exception e) {
            CLog.e(e.getMessage());
            return null;
        }
    }

    /**
     * request
     *
     * @param apiCallback
     */
    public void request(int schdlId, long sendRowId, final APIManager.APICallback apiCallback) {
        try {
            apiManager.call(API_SET_ARRIVE, getParam(schdlId, sendRowId), new APIManager.APICallback() {
                @Override
                public void response(String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            CLog.e(e.getMessage());
        }
    }

    /**
     * request
     *
     * @param apiCallback
     */
    // 기존 3번 retry 하는 구문 제외하고 보내고 우선 GS에 전달, 추후 테스트 되면 적용하여 전달 예정
    /*
    public void request(final int schdlId, final long sendRowId,
                        final APIManager.APICallback apiCallback) {
        new AsyncTask<Void, Object, Boolean>() {

            // 최대 세번 API_SET_ARRIVE 수행 성공 시 종료
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    mCheckNum = 0;

                    // 최대 NUM_CHECK_ARRIVE_CNT 만큼 수행
                    while (mCheckNum < NUM_CHECK_ARRIVE_CNT) {
                        try {
                            Thread.sleep(NUM_CHECK_ARRIVE_DELAY);
                        } catch (InterruptedException e) {
                            CLog.e("Arrive check delay failed");
                        }

                        apiManager.call(API_SET_ARRIVE, getParam(schdlId, sendRowId), new APIManager.APICallback() {
                            @Override
                            public void response(String code, JSONObject json) {
                                if (CODE_SUCCESS.equals(code)) {
                                    // 성공 코드일 경우 NUM_CHECK_ARRIVE_CNT 만큼 call 하지 않고 종료.
                                    mCheckNum = NUM_CHECK_ARRIVE_CNT;
                                    requiredResultProc(json);
                                }
//                                if (apiCallback != null) {
//                                    apiCallback.response(code, json);
//                                }
                                // 성공이 아닐 시에도 response 보내 주어야 한다
                                publishProgress(code, json);
                            }
                        });
                        mCheckNum ++;
                    }
                    return true;
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                    return false;
                }
            }

            @Override
            protected void onProgressUpdate (Object... values) {
                if (apiCallback != null) {
                    apiCallback.response((String) values[0], (JSONObject) values[1]);
                }
            };
        }.execute();
    }
*/
    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(JSONObject json) {
        return true;
    }
}
