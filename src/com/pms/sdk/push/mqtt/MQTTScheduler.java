package com.pms.sdk.push.mqtt;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

import java.util.List;

/**
 * Created by LimHeekang on 2017. 11. 28..
 */

public class MQTTScheduler implements IPMSConsts {
    private Context mContext = null;
    MQTTScheduler(Context context) {
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setJobService(String action) {
        int nJobID = JOB_SCHEDULER_ID;
        try {
            nJobID = Integer.parseInt(PMSUtil.getGCMProjectId(mContext));
        }
        catch (NumberFormatException e) {
            CLog.e(e.getMessage());
        }

        JobInfo.Builder info = new JobInfo.Builder(nJobID,
                new ComponentName(mContext.getApplicationContext(), MQTTJobService.class));
        info.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
//        info.setPeriodic(JOB_SCHEDULER_INTERVAL);
        info.setMinimumLatency(JOB_SCHEDULER_INTERVAL);

        PersistableBundle bundle = new PersistableBundle();
        bundle.putString(ACTION_MQTT_BUNDLE, action);
        info.setExtras(bundle);
        JobScheduler scheduler = (JobScheduler) mContext.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        CLog.d("MQTTScheduler setJobService : schedule");
        scheduler.schedule(info.build());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void cancelAllJobs() {
        JobScheduler scheduler = (JobScheduler) mContext.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.cancelAll();
    }

    /**
     * Executed when user clicks on FINISH LAST TASK.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void finishJobService(int jobID) {
        JobScheduler jobScheduler = (JobScheduler) mContext.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        List<JobInfo> allPendingJobs = jobScheduler.getAllPendingJobs();
        if (allPendingJobs.size() > 0) {
            for (JobInfo info : allPendingJobs) {
                if (info.getId() == jobID) {
                    jobScheduler.cancel(jobID);
                    break;
                }
            }
        }
    }
}
