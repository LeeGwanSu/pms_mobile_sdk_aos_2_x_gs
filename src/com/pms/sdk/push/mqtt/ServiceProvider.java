package com.pms.sdk.push.mqtt;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;

/**
 * Created by LimHeekang on 2017. 11. 29..
 */

public class ServiceProvider implements IPMSConsts {
    private static MQTTScheduler mScheduler;
    public ServiceProvider(Context context) {
        mScheduler = new MQTTScheduler(context);
    }
    private static final boolean IS_JOBSERVICE_ON = true;

    public static void startService(Context context, String strAction) {
        if (IS_JOBSERVICE_ON) {
            int ver = Build.VERSION.SDK_INT;
            if (ver > Build.VERSION_CODES.N_MR1) {
                if (mScheduler == null)
                    mScheduler = new MQTTScheduler(context);
                CLog.d("mScheduler.setJobService()");
                mScheduler.setJobService(strAction);
            } else {
                Intent i = new Intent(context, MQTTService.class);
                i.setAction(strAction);
                context.startService(i);
            }
        }
        else {
            Intent i = new Intent(context, MQTTService.class);
            i.setAction(strAction);
            context.startService(i);
        }
    }

    public static void stopService(Context context, String strAction) {
        if (IS_JOBSERVICE_ON) {
            int ver = Build.VERSION.SDK_INT;
            if (ver > Build.VERSION_CODES.N_MR1) {
                if (mScheduler == null)
                    mScheduler = new MQTTScheduler(context);
                mScheduler.finishJobService(JOB_SCHEDULER_ID);
            } else {
                Intent i = new Intent(context, MQTTService.class);
                if (strAction != null)
                    i.setAction(strAction);
                context.stopService(i);
            }
        }
        else {
            Intent i = new Intent(context, MQTTService.class);
            if (strAction != null)
                i.setAction(strAction);
            context.stopService(i);
        }
    }

    public static void actionNetworkChangeRestart(Context context) {
        if (IS_JOBSERVICE_ON) {
            int ver = Build.VERSION.SDK_INT;
            if (ver > Build.VERSION_CODES.N_MR1) {
                MQTTJobService.actionNetworkChangeRestart(context.getApplicationContext());
            } else {
                MQTTService.actionNetworkChangeRestart(context.getApplicationContext());
            }
        }
        else {
            MQTTService.actionNetworkChangeRestart(context.getApplicationContext());
        }
    }
}
