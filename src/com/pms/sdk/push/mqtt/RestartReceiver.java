package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.FileUtil;
import com.pms.sdk.common.util.PMSUtil;

/**
 * Private PUSH Restart Receiver
 * 
 * @author erzisk
 * 
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String TAG = "MQTT";

	@Override
	public synchronized void onReceive (Context context, Intent intent) {
		CLog.d(TAG, "onReceive() " + intent);
//		FileUtil fileUtil = new FileUtil();
//		try {
//			fileUtil.getConnectLogUtil(context, 5, ((intent.getAction() != null) ? intent.getAction() : "null"));
//		} catch (Exception e) {
//			CLog.e(e.getMessage());
//		}

		CLog.i(TAG, "MQTT_FLAG -> " + PMSUtil.getMQTTFlag(context) + ", PRIVATE_FLAG -> " + PMSUtil.getPrivateFlag(context));
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context)) && FLAG_Y.equals(PMSUtil.getPrivateFlag(context))) {
			String action = intent.getAction();
			if (action == null) {
				int ver = Build.VERSION.SDK_INT;
				if (ver > Build.VERSION_CODES.N_MR1) {
					ServiceProvider.startService(context, ACTION_START);
				}
				else {
					MQTTService.actionStart(context.getApplicationContext());
				}
			} else {
				if (action.equals(ACTION_BOOT_COMPLETED) || action.equals(ACTION_ACTION_PACKAGE_RESTARTED)) {
					int ver = Build.VERSION.SDK_INT;
					if (ver > Build.VERSION_CODES.N_MR1) {
						ServiceProvider.startService(context, ACTION_START);
					}
					else {
						MQTTService.actionStart(context.getApplicationContext());
					}
				} else if (action.equals(ACTION_CHANGERECONNECT)) {
					ServiceProvider.actionNetworkChangeRestart(context.getApplicationContext());
				} else if (action.equals(ACTION_STOP)) {
					ServiceProvider.stopService(context.getApplicationContext(), null);
				} else if (action.equals(ACTION_USER_PRESENT)) {
					ServiceProvider.startService(context, ACTION_FORCE_START);
				} else if(action.equals(ACTION_FORCE_START)){
					ServiceProvider.startService(context, ACTION_FORCE_START);
				}
			}
		}
	}
}
