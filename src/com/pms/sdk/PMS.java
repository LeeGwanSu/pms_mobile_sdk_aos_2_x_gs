package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.17 13:32] MQTT Client Service 중복 실행 체크 & ConnectionChangeReceiver 추가<br>
 *          [2013.12.19 09:02] MQTT Client ReConnect Timing 변경 & ConnectionChangeReceiver 변경<br>
 *          [2013.12.19 17:43] MQTT Client Keep Alive 시간 랜덤으로 적용 & DeviceCert시 PrivateFlag 루틴 수정함. <br>
 *          [2013.12.27 13:08] GCM regist 시에 GCM_PACKAGE 전송 <br>
 *          [2013.12.27 14:53] DeviceCert 시 private Flag 처리 <br>
 *          [2014.01.17 17:30] ReadMsg 파라미터 변경 <br>
 *          [2014.05.08 16:27] API & MQTT Client 최신버전으로 적용함 <br>
 *          [2014.05.21 16:09] MSG DB 정보 변경함.<br>
 *          [2014.06.05 12:49] ReadMsg 버그 수정함.<br>
 *          [2014.06.17 20:00] 다른앱 실행시 팝업 안뜨게 수정 & 버그 수정함.<br>
 *          [2014.06.19 10:54] 팝업창에 대해서 디폴트값 메타데이터로 설정 하게 수정함. <br>
 *          [2014.07.03 11:55] MQTT 죽는 부분 예외처리함. <br>
 *          [2014.07.14 12:22] 팝업창에 대한 설정 키값 변경함. <br>
 *          [2014.07.24 11:20] newMSg 시 pushImg 저장되게 수정함. <br>
 *          [2014.07.25 10:50] 자동 ReadMsg 104 오류발생시 JSON 재구성해서 다시 호출하게 수정함. <br>
 *          [2014.07.30 10:32] pushImg에 대한 버그 수정함. <br>
 *          [2014.08.21 10:25] msgFlag & notiFlag를 디바이스 기준으로 변경함. <br>
 *          [2014.09.11 10:21] Notification Icon을 블러오는 위치를 Androidmanifest.xml 쪽으로 바꿈. <br>
 *          [2014.09.11 15:17] Volley No Cache 변경, Notification Bar Priority Level 2로 변경함. <br>
 *          [2014.09.19 10:13] DeviceUid & WaPcId 값을 추가함. <br>
 *          [2014.09.25 10:31] Notification Image 다운로드 실패시 text style로 변환루틴 추가함. <br>
 *          [2014.10.27 13:52] 몌세지 중복처리 & DB 값 추가함. <br>
 *          [2014.10.29 11:30] 메세지 중복처리 & DB Insert 문 수정함. <br>
 *          [2014.11.04 14:17] 메세지 Insert시 버그 수정함. <br>
 *          [2014.11.10 18:03] User Data를 무조건 Null 값으로 처리함. <br>
 *          [2014.11.19 16:27] runningTask() 사용하는 부분 삭제함. <br>
 *          [2014.12.18 13:43] UUID를 넣을수 있게 수정함. <br>
 *          [2015.01.30 13:34] GCM 요청부분을 주석처리함. Token 값을 넣을 수 있게 수정함. <br>
 *          [2015.03.09 11:06] GCM 요청부분을 주석해제함. MQTT Client 버전업함. <br>
 *          [2015.03.13 11:42] GCM요청하게 수정함. <br>
 *          [2015.07.14 14:26] Google ID 넣을수 있도록 수정함. <br>
 *          [2015.08.26 09:57] GCM 요청부분을 주석처리함. <br>
 *          [2015.08.27 15:45] PMSDB NullPointException 오류나는 부분 수정함.<br>
 *          [2015.08.28 14:36] GCM호출하는 부분외에는 주석해제<br>
 *          [2016.04.21 16:41] GetSingKey 적용함. <br>
 *          [2016.05.03 13:35] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.09.14 13:30] UUID 관련 수정 , UUID값 가져오는 기능추가 ,Bigtext mode 추가, file권한 에러 버그 수정<br>
 *          [2017.10.18 19:50] Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					DB 중복 Open으로 인해 강제종료 문제 수정
 *          					PushPopup 발생 시 강제 종료 문제 수정 <br>
 *          [2017.10.20 16:28] UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2017.11.03 09:46] PMS_ARRIVE_FLAG 로 도달률 발송할 지 여부 결정 <br>
 *          [2017.11.21 16:46] 페이로드 값에 PCID추가 setPCID 함수 추가, 해당 함수로 PCID 설정 <br>
 *          [2018.09.13 11:04] Android 8.0 대응, 이미지 캐시 대응, 잠금화면 체크, 벨소리대응, Intent 대응, WRITE_STORAGE_PERMISSION 제거
 *          [2018.09.20 14:17] UUID 권한 필요없는걸로 적용
 *          [2018.11.13 11:54] 벨소리 채널 대응
 *          [2018.12.28 16:52] FCM 대응, HTML 푸시 대응, 벨소리 볼륨 조절 개선, FCM Deprecated 대응
 *          [2019.01.17 18:33] FCMInstanceId Deprecated 대응, NotiBackColor 추가
 *          [2019.03.13 11:00] 업체요청으로 기생성된 채널 있을 시 채널 변경 하지 않음
 *          [2019.03.15 11:24] 업체요청으로 NotificationChannel 이름, ID 설정하도록 함, 벨소리 채널 볼륨 오류 수정
 *          [2019.03.22 16:27] setColor 버전 분기 처리, 불필요한 토큰 요청 제거
 *          [2020.08.21 14:28] 업체요청으로 아이콘 설정시 meta-data의 값을 가져와 설정하도록 변경
 */
public class PMS implements IPMSConsts {

	private static PMS instance;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:202008211428");

		initOption(context);
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_POPUP_FLAG))) {
			mPrefs.putString(PREF_ALERT_POPUP_FLAG, "N");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CANCEL_TEXT))) {
			mPrefs.putString(PREF_POPUP_CANCEL_TEXT, "CLOSE");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT))) {
			mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, "OK");
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		// CLog.d("getInstance");
		if (instance == null) {
			instance = new PMS(context);
		}
		instance.setmContext(context);
		return instance;
	}

	/**
	 * start mqtt service
	 */
	public void startMQTTService (Context context) {
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
			context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
		} else {
			context.stopService(new Intent(context, MQTTService.class));
		}
	}

	public void stopMQTTService (Context context) {
		context.stopService(new Intent(context, MQTTService.class));
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instance.mContext, DEVICECERT_PENDING);
			instance.unregisterReceiver();
			instance = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	// /**
	// * push token 세팅
	// * @param pushToken
	// */
	// public void setPushToken(String pushToken) {
	// CLog.i(TAG + "setPushToken");
	// CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
	// PMSUtil.setGCMToken(mContext, pushToken);
	// }

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	public void setGCMToken (String token) {
		PMSUtil.setGCMToken(mContext, token);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setDeviceId (String deviceid) {
		PMSUtil.setDeviceId(mContext, deviceid);
	}

	public void setWapcId (String wapcid) {
		PMSUtil.setWapcId(mContext, wapcid);
	}

	public void setUUID (String uuid) {
		PMSUtil.setUUID(mContext, uuid);
	}

	public String getUUID () {
		return PMSUtil.getUUID(mContext);
	}

	public void setGoogleID (String googleid) {
		PMSUtil.setGoogleID(mContext, googleid);
	}

	public String getGoogleID () {
		return PMSUtil.getGoogleID(mContext);
	}

	public void setPCID (String pcid) {
		PMSUtil.setPcId(mContext, pcid);
	}

	public String getPCID () {
		return PMSUtil.getPcId(mContext);
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * set pushPopup color
	 * 
	 * @param pushPopupColor
	 */
	public void setPushPopupColor (int pushPopupColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_COLOR, pushPopupColor);
	}

	/**
	 * set pushPopup background color
	 * 
	 * @param pushPopupBackgroundColor
	 */
	public void setPushPopupBackgroundColor (int pushPopupBackgroundColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_BACKGROUND_COLOR, pushPopupBackgroundColor);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * set noti icon
	 * 
	 * @param resId
	 */
	@Deprecated
	public void setNotiIcon (int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	@Deprecated
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_POPUP_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}

	public void bindBadge (Context c, int id) {
//		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge) {
//		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
		Intent i = new Intent(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public Msg selectMsg (String userMsgId) {
		return mDB.selectMsg(userMsgId);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteMsg (String userMsgId) {
		return mDB.deleteMsg(userMsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	/**
	 * set popup confirm text
	 * 
	 * @param confirmText
	 */
	public void setPopupConfirmText (String confirmText) {
		mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, confirmText);
	}

	/**
	 * set popup cancel text
	 * 
	 * @param cancelText
	 */
	public void setPopupCancelText (String cancelText) {
		mPrefs.putString(PREF_POPUP_CANCEL_TEXT, cancelText);
	}

	public void setNotiReceiverClass (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentAction);
	}
	public void setPushReceiverClass (String intentAction) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentAction);
	}

	public void setNotiGroupFlag(boolean isNotiGroup)
	{
		mPrefs.putString(IPMSConsts.PREF_NOTI_GROUP_FLAG, isNotiGroup?"Y":"N");
	}
	public String getNotiGroupFlag()
	{
		return mPrefs.getString(IPMSConsts.PREF_NOTI_GROUP_FLAG);
	}


}
